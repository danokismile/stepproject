let tab = function (){
    let tabNav = document.querySelectorAll('.tabs-nav_item'),
        tabContent = document.querySelectorAll('.tab'),
        tabName;
    tabNav.forEach(item =>{
        item.addEventListener('click', selectTabNav)
    });
    function selectTabNav(){
        tabNav.forEach(item=>{
            item.classList.remove('is-active')
        });
        this.classList.add('is-active');
        tabName = this.getAttribute('data-tab-name');
        selectTabContent(tabName);
    }
    function selectTabContent(tabName){
        tabContent.forEach(item=>{
            item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
        });
    }
};
tab();


let listWork = document.querySelector('.list-work')
listWork.addEventListener('click', filterTabs)
function filterTabs(event){
    let portfolio = [...document.querySelector('.portfolio-list').children]
    let targeShow = null
    if (event){
       targeShow = event.target
        targeShow.closest('ul').querySelector('.active').classList.remove('active')
        targeShow.classList.add('active')
    }else{
        targeShow = document.querySelector('.active')
    }

    portfolio.forEach(item =>{
        item.setAttribute('hidden', true)
        if(item.dataset.filter === targeShow.id){
            console.log(item)
            item.removeAttribute('hidden')
        }else if(targeShow.id === Filter){
            item.removeAttribute('hidden')
        }
    })
}


$(document).ready(function (){
    $('.avatar-start').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: ".logo-menu"
    });
    $('.logo-menu').slick({
        centerMode: true,
        focusOnSelect: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor:".avatar-start",
        speed:500
    });

});


$(document).ready(function (){
    jQuery('.menu-average').hover(
        function (){
           $(this).find(".cover-item").fadeIn();
        },
        function (){
                $(this).find(".cover-item").fadeOut();
            }
    );
    let sizer = '.sizer4';
    let container =  $('#gallery');
    container.imagesLoaded(function (){
        container.masonry({
    itemSelector: '.menu-average',
            columnWidth: sizer,
            percentPosition: true
        });
    });
});


let iconAll = document.querySelectorAll('.icon');
let iconList = document.querySelector('.portfolio-list');
let button = document.querySelector('.color-amazing');
let Filter = 'All'
let img = 12;
let show = []
let arr =[{
        src: "./img/graphic%20design/graphic-design1.jpg",
        text: "Graphic Design",
    },
    {
        src: "./img/web%20design/web-design3.jpg",
        text: "Web Design",
    },
    {
        src: "./img/landing%20page/landing-page1.jpg",
        text: "Landing Pages",
    },
    {
        src: "./img/wordpress/wordpress1.jpg",
        text: "Wordpress",
    },
    {
        src: "./img/graphic%20design/graphic-design2.jpg",
        text: "Graphic Design",
    },
    {
        src: "./img/web%20design/web-design4.jpg",
        text: "Web Design",
    },
    {
        src: "./img/landing%20page/landing-page2.jpg",
        text: "Landing Pages",
    },
    {
        src: "./img/wordpress/wordpress3.jpg",
        text: "Wordpress",
    },
    {
        src: "./img/graphic%20design/graphic-design3.jpg",
        text: "Graphic Design",
    },
    {
        src: "./img/web%20design/web-design6.jpg",
        text: "Web Design",
    },
    {
        src: "./img/landing%20page/landing-page5.jpg",
        text: "Landing Pages",
    },
    {
        src: "./img/wordpress/wordpress4.jpg",
        text: "Wordpress",
    },

    {
        src: "./img/graphic%20design/graphic-design4.jpg",
        text: "Graphic Design",
    },
    {
        src: "./img/web%20design/web-design7.jpg",
        text: "Web Design",
    },
    {
        src: "./img/landing%20page/landing-page6.jpg",
        text: "Landing Pages",
    },
    {
        src: "./img/wordpress/wordpress6.jpg",
        text: "Wordpress",
    },
    {
        src: "./img/graphic%20design/graphic-design8.jpg",
        text: "Graphic Design",
    },
    {
        src: "./img/graphic%20design/graphic-design7.jpg",
        text: "Graphic Design",
    },
    {
        src: "./img/graphic%20design/graphic-design12.jpg",
        text: "Graphic Design",
    },
    {
        src: "./img/wordpress/wordpress8.jpg",
        text: "Wordpress",
    },
    {
        src: "./img/graphic%20design/graphic-design11.jpg",
        text: "Graphic Design",
    },
    {
        src: "./img/graphic%20design/graphic-design9.jpg",
        text: "Graphic Design",
    },
    {
        src: "./img/wordpress/wordpress10.jpg",
        text: "Wordpress",
    },
    {
        src: "./img/wordpress/wordpress9.jpg",
        text: "Wordpress",
    },
]
function render(array){
    iconList.insertAdjacentHTML('beforeend', `${array.map(item =>{
       return `<li data-filter="${item.text}" class="icon">
                         <img src="${item.src}" alt="">
                       <div class="page-style">
                           <div class="ball-all">
                               <a href=""><div class="ball-green"><img src="./img/16.png" alt="hitch" class="hitch"></div></a>
                               <a href=""><div class="ball-border">
                                   <div class="ball-white"></div>
                               </div></a>
                           </div>
                           <a href="" class="page-text"><h3>creative design <a href="" class="page-span"><br>Web Design</a></h3></a>
                       </div>
                   </li>` 
    }).join('')
    }`)
}
function renderImg(){
    show = arr.splice(0, img)
    console.log(show)
    render(show)
    filterTabs()
}
button.addEventListener('click', (event) =>{
    if (img >= 36){
        button.remove()
    }
    renderImg()
    $('.color-amazing').click(function() {
        $(".color-amazing").css("display", "none");
    });
});
